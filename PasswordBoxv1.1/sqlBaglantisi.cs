﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;


namespace PasswordBoxv1._1
{
    class sqlBaglantisi
    {
        private string kayit;
        private string sifreliMetin;
        Hata dbHata = new Hata();
        private static string conString="Data Source=.; Initial Catalog=Pwbox; Integrated Security=true";
      
        SqlConnection baglanti = new SqlConnection(conString);
        

        public void user_add(string no,string mail)
        {
                try
                {
                    if (baglanti.State==ConnectionState.Closed)
                        baglanti.Open();

                 kayit = ("INSERT INTO [User] (telefonNo, mail) VALUES (@no , @mail)");
                 SqlCommand komut = new SqlCommand(kayit, baglanti);
                 komut.Parameters.AddWithValue("@no", no);
                 komut.Parameters.AddWithValue("@mail", mail);
                 komut.ExecuteNonQuery();
                 MessageBox.Show("Kaydınız Başarı ile Alınmıştır.");
                }

                catch (Exception hata)
                {
                 MessageBox.Show("İşlem Sırasında Hata oluştu... " + hata.Message);
                }

                finally
                {
                 baglanti.Close();
                }
            
        }

        public int user_login(string code,string activation)
        {
            if (code == activation)
            {
                MessageBox.Show("Giriş Başarılı");
                return 1;
            }
            else
            {
                MessageBox.Show("Giriş Başarısız Doğrulama Kodu Yanlış");
                return 0;
            }
        }


        public bool SifreKaydet(string sifre, string tNo, string paswordName, string paswordID)
        {
            string userNo = tNo;
            bool sonuc = false;

            kayit = ("INSERT INTO [password] (userNo,password,paswordName,paswordID) Values(@userNo,@password,@paswordName,@paswordID) ");
            SqlCommand komut = new SqlCommand(kayit, baglanti);
            try
            {
                if (baglanti.State == ConnectionState.Closed)
                    baglanti.Open();

                komut.Parameters.AddWithValue("@userNO", userNo);
                komut.Parameters.AddWithValue("@password", sifre);
                komut.Parameters.AddWithValue("@paswordName", paswordName);
                komut.Parameters.AddWithValue("@paswordID", paswordID);

                sonuc = Convert.ToBoolean(komut.ExecuteNonQuery());

            }
            catch (SqlException ex)
            {
                dbHata.hataGonderMesaj(ex.Message);

            }
            finally
            {
                baglanti.Close();
            }

            return sonuc;
        }


        public string get_pwd_where_pwdName(string pwdName)
        {
           
            kayit = "Select password From [password] where paswordName='"+pwdName+"'";
            
            try
            {
                if(baglanti.State == ConnectionState.Closed)
                    baglanti.Open();

                SqlCommand komut = new SqlCommand(kayit, baglanti);
                SqlDataReader dr = komut.ExecuteReader();
                
             
                if (dr.Read())
                {
                    dr.Close();
                    sifreliMetin = (string)komut.ExecuteScalar();
                    

                }

            }
            catch (SqlException ex)
            {

                dbHata.hataGonderMesaj("İşlem yapılamadı hata= " + ex.Message + "..!");
               

            }
            finally
            {
                baglanti.Close();
            }



            return sifreliMetin;
        }
        

        public string get_mail_where_tNo(string no)
        {
            string mailAdres;
            mailAdres = "";
            try
            {
                if (baglanti.State == ConnectionState.Closed)
                    baglanti.Open();

                kayit = ("select mail from [User] where telefonNo='" + no + "'");
                SqlCommand komut = new SqlCommand(kayit, baglanti);
                SqlDataReader dr = komut.ExecuteReader();
                if (dr.Read())
                {

                    
                    mailAdres = dr.GetString(0).ToString();
                   
                    
                }
              else
                {
                    MessageBox.Show("Hatalı Telefon Numarası!!!");                                


                }
                
            }
            catch (SqlException sqlhata)
            {

                dbHata.hataGonderMesaj(sqlhata.Message.ToString());
            }
            finally
            {
                
                baglanti.Close();

            }
            return mailAdres;

        }
    }
}
