﻿namespace PasswordBoxv1._1
{
    partial class sifre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_s_mail = new System.Windows.Forms.TextBox();
            this.lbl_s_mail = new System.Windows.Forms.Label();
            this.btb_s_sifrele = new System.Windows.Forms.Button();
            this.chckbx_Affine = new System.Windows.Forms.CheckBox();
            this.grpbx_sifre = new System.Windows.Forms.GroupBox();
            this.lbl_sifrele_plat = new System.Windows.Forms.Label();
            this.pwdName = new System.Windows.Forms.ComboBox();
            this.lbl_text_no = new System.Windows.Forms.Label();
            this.lbl_telNo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_s_key2 = new System.Windows.Forms.TextBox();
            this.txt_s_key1 = new System.Windows.Forms.TextBox();
            this.chckbc_B_F = new System.Windows.Forms.CheckBox();
            this.chckbx_RSA = new System.Windows.Forms.CheckBox();
            this.txt_s_sifre = new System.Windows.Forms.TextBox();
            this.lbl_s_sifre = new System.Windows.Forms.Label();
            this.txt_sifrecoz_key1 = new System.Windows.Forms.TextBox();
            this.lbl_sifrecozkey1 = new System.Windows.Forms.Label();
            this.txt_sifrecoz_key2 = new System.Windows.Forms.TextBox();
            this.cbx_sifrecoz_plat = new System.Windows.Forms.ComboBox();
            this.lbl_sifrecoz_key2 = new System.Windows.Forms.Label();
            this.lbl_sifrecoz_plat = new System.Windows.Forms.Label();
            this.btn_sifre_coz = new System.Windows.Forms.Button();
            this.btn_girisYap = new System.Windows.Forms.Button();
            this.grpbx_sifre.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_s_mail
            // 
            this.txt_s_mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_s_mail.Location = new System.Drawing.Point(395, 37);
            this.txt_s_mail.Name = "txt_s_mail";
            this.txt_s_mail.Size = new System.Drawing.Size(134, 23);
            this.txt_s_mail.TabIndex = 0;
            // 
            // lbl_s_mail
            // 
            this.lbl_s_mail.AutoSize = true;
            this.lbl_s_mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_s_mail.Location = new System.Drawing.Point(308, 43);
            this.lbl_s_mail.Name = "lbl_s_mail";
            this.lbl_s_mail.Size = new System.Drawing.Size(81, 17);
            this.lbl_s_mail.TabIndex = 1;
            this.lbl_s_mail.Text = "Mail Adresi:";
            this.lbl_s_mail.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btb_s_sifrele
            // 
            this.btb_s_sifrele.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btb_s_sifrele.Location = new System.Drawing.Point(454, 117);
            this.btb_s_sifrele.Name = "btb_s_sifrele";
            this.btb_s_sifrele.Size = new System.Drawing.Size(75, 31);
            this.btb_s_sifrele.TabIndex = 2;
            this.btb_s_sifrele.Text = "Şifrele";
            this.btb_s_sifrele.UseVisualStyleBackColor = true;
            this.btb_s_sifrele.Click += new System.EventHandler(this.btb_s_sifrele_Click);
            // 
            // chckbx_Affine
            // 
            this.chckbx_Affine.AutoSize = true;
            this.chckbx_Affine.Location = new System.Drawing.Point(46, 25);
            this.chckbx_Affine.Name = "chckbx_Affine";
            this.chckbx_Affine.Size = new System.Drawing.Size(53, 17);
            this.chckbx_Affine.TabIndex = 3;
            this.chckbx_Affine.Text = "Affine";
            this.chckbx_Affine.UseVisualStyleBackColor = true;
            this.chckbx_Affine.CheckedChanged += new System.EventHandler(this.chckbx_Affine_CheckedChanged);
            // 
            // grpbx_sifre
            // 
            this.grpbx_sifre.Controls.Add(this.lbl_sifrele_plat);
            this.grpbx_sifre.Controls.Add(this.pwdName);
            this.grpbx_sifre.Controls.Add(this.lbl_text_no);
            this.grpbx_sifre.Controls.Add(this.lbl_telNo);
            this.grpbx_sifre.Controls.Add(this.label2);
            this.grpbx_sifre.Controls.Add(this.label1);
            this.grpbx_sifre.Controls.Add(this.txt_s_key2);
            this.grpbx_sifre.Controls.Add(this.txt_s_key1);
            this.grpbx_sifre.Controls.Add(this.chckbc_B_F);
            this.grpbx_sifre.Controls.Add(this.chckbx_RSA);
            this.grpbx_sifre.Controls.Add(this.txt_s_sifre);
            this.grpbx_sifre.Controls.Add(this.btb_s_sifrele);
            this.grpbx_sifre.Controls.Add(this.lbl_s_sifre);
            this.grpbx_sifre.Controls.Add(this.txt_s_mail);
            this.grpbx_sifre.Controls.Add(this.chckbx_Affine);
            this.grpbx_sifre.Controls.Add(this.lbl_s_mail);
            this.grpbx_sifre.Location = new System.Drawing.Point(3, 225);
            this.grpbx_sifre.Name = "grpbx_sifre";
            this.grpbx_sifre.Size = new System.Drawing.Size(751, 175);
            this.grpbx_sifre.TabIndex = 4;
            this.grpbx_sifre.TabStop = false;
            this.grpbx_sifre.Text = "Şifreleme İçin 1 Tane Seçin";
            // 
            // lbl_sifrele_plat
            // 
            this.lbl_sifrele_plat.AutoSize = true;
            this.lbl_sifrele_plat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_sifrele_plat.Location = new System.Drawing.Point(543, 78);
            this.lbl_sifrele_plat.Name = "lbl_sifrele_plat";
            this.lbl_sifrele_plat.Size = new System.Drawing.Size(32, 17);
            this.lbl_sifrele_plat.TabIndex = 21;
            this.lbl_sifrele_plat.Text = "Site";
            this.lbl_sifrele_plat.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pwdName
            // 
            this.pwdName.FormattingEnabled = true;
            this.pwdName.Items.AddRange(new object[] {
            "Facebook",
            "Hotmail",
            "Piazza",
            "BitBucket"});
            this.pwdName.Location = new System.Drawing.Point(607, 76);
            this.pwdName.Margin = new System.Windows.Forms.Padding(2);
            this.pwdName.Name = "pwdName";
            this.pwdName.Size = new System.Drawing.Size(108, 21);
            this.pwdName.TabIndex = 17;
            // 
            // lbl_text_no
            // 
            this.lbl_text_no.AutoSize = true;
            this.lbl_text_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_text_no.Location = new System.Drawing.Point(543, 43);
            this.lbl_text_no.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_text_no.Name = "lbl_text_no";
            this.lbl_text_no.Size = new System.Drawing.Size(57, 17);
            this.lbl_text_no.TabIndex = 16;
            this.lbl_text_no.Text = "Tel NO:";
            this.lbl_text_no.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbl_telNo
            // 
            this.lbl_telNo.AutoSize = true;
            this.lbl_telNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_telNo.Location = new System.Drawing.Point(604, 43);
            this.lbl_telNo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_telNo.Name = "lbl_telNo";
            this.lbl_telNo.Size = new System.Drawing.Size(57, 17);
            this.lbl_telNo.TabIndex = 16;
            this.lbl_telNo.Text = "Tel NO:";
            this.lbl_telNo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(78, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Key 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(78, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Key 1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txt_s_key2
            // 
            this.txt_s_key2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_s_key2.Location = new System.Drawing.Point(128, 102);
            this.txt_s_key2.Name = "txt_s_key2";
            this.txt_s_key2.Size = new System.Drawing.Size(134, 23);
            this.txt_s_key2.TabIndex = 11;
            // 
            // txt_s_key1
            // 
            this.txt_s_key1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_s_key1.Location = new System.Drawing.Point(128, 60);
            this.txt_s_key1.Name = "txt_s_key1";
            this.txt_s_key1.Size = new System.Drawing.Size(134, 23);
            this.txt_s_key1.TabIndex = 10;
            // 
            // chckbc_B_F
            // 
            this.chckbc_B_F.AutoSize = true;
            this.chckbc_B_F.Location = new System.Drawing.Point(191, 25);
            this.chckbc_B_F.Name = "chckbc_B_F";
            this.chckbc_B_F.Size = new System.Drawing.Size(71, 17);
            this.chckbc_B_F.TabIndex = 9;
            this.chckbc_B_F.Text = "Blow Fish";
            this.chckbc_B_F.UseVisualStyleBackColor = true;
            this.chckbc_B_F.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // chckbx_RSA
            // 
            this.chckbx_RSA.AutoSize = true;
            this.chckbx_RSA.Location = new System.Drawing.Point(117, 25);
            this.chckbx_RSA.Name = "chckbx_RSA";
            this.chckbx_RSA.Size = new System.Drawing.Size(48, 17);
            this.chckbx_RSA.TabIndex = 8;
            this.chckbx_RSA.Text = "RSA";
            this.chckbx_RSA.UseVisualStyleBackColor = true;
            this.chckbx_RSA.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txt_s_sifre
            // 
            this.txt_s_sifre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt_s_sifre.Location = new System.Drawing.Point(395, 74);
            this.txt_s_sifre.Name = "txt_s_sifre";
            this.txt_s_sifre.PasswordChar = '*';
            this.txt_s_sifre.Size = new System.Drawing.Size(134, 23);
            this.txt_s_sifre.TabIndex = 5;
            // 
            // lbl_s_sifre
            // 
            this.lbl_s_sifre.AutoSize = true;
            this.lbl_s_sifre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_s_sifre.Location = new System.Drawing.Point(308, 77);
            this.lbl_s_sifre.Name = "lbl_s_sifre";
            this.lbl_s_sifre.Size = new System.Drawing.Size(59, 17);
            this.lbl_s_sifre.TabIndex = 4;
            this.lbl_s_sifre.Text = "Şifreniz:";
            this.lbl_s_sifre.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txt_sifrecoz_key1
            // 
            this.txt_sifrecoz_key1.Location = new System.Drawing.Point(84, 50);
            this.txt_sifrecoz_key1.Name = "txt_sifrecoz_key1";
            this.txt_sifrecoz_key1.Size = new System.Drawing.Size(123, 20);
            this.txt_sifrecoz_key1.TabIndex = 5;
            // 
            // lbl_sifrecozkey1
            // 
            this.lbl_sifrecozkey1.AutoSize = true;
            this.lbl_sifrecozkey1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_sifrecozkey1.Location = new System.Drawing.Point(22, 53);
            this.lbl_sifrecozkey1.Name = "lbl_sifrecozkey1";
            this.lbl_sifrecozkey1.Size = new System.Drawing.Size(44, 17);
            this.lbl_sifrecozkey1.TabIndex = 18;
            this.lbl_sifrecozkey1.Text = "Key 1";
            this.lbl_sifrecozkey1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txt_sifrecoz_key2
            // 
            this.txt_sifrecoz_key2.Location = new System.Drawing.Point(84, 88);
            this.txt_sifrecoz_key2.Name = "txt_sifrecoz_key2";
            this.txt_sifrecoz_key2.Size = new System.Drawing.Size(123, 20);
            this.txt_sifrecoz_key2.TabIndex = 20;
            // 
            // cbx_sifrecoz_plat
            // 
            this.cbx_sifrecoz_plat.FormattingEnabled = true;
            this.cbx_sifrecoz_plat.Items.AddRange(new object[] {
            "Facebook",
            "Hotmail",
            "Piazza",
            "BitBucket"});
            this.cbx_sifrecoz_plat.Location = new System.Drawing.Point(284, 52);
            this.cbx_sifrecoz_plat.Margin = new System.Windows.Forms.Padding(2);
            this.cbx_sifrecoz_plat.Name = "cbx_sifrecoz_plat";
            this.cbx_sifrecoz_plat.Size = new System.Drawing.Size(108, 21);
            this.cbx_sifrecoz_plat.TabIndex = 18;
            // 
            // lbl_sifrecoz_key2
            // 
            this.lbl_sifrecoz_key2.AutoSize = true;
            this.lbl_sifrecoz_key2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_sifrecoz_key2.Location = new System.Drawing.Point(22, 88);
            this.lbl_sifrecoz_key2.Name = "lbl_sifrecoz_key2";
            this.lbl_sifrecoz_key2.Size = new System.Drawing.Size(44, 17);
            this.lbl_sifrecoz_key2.TabIndex = 19;
            this.lbl_sifrecoz_key2.Text = "Key 1";
            this.lbl_sifrecoz_key2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbl_sifrecoz_plat
            // 
            this.lbl_sifrecoz_plat.AutoSize = true;
            this.lbl_sifrecoz_plat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_sifrecoz_plat.Location = new System.Drawing.Point(233, 53);
            this.lbl_sifrecoz_plat.Name = "lbl_sifrecoz_plat";
            this.lbl_sifrecoz_plat.Size = new System.Drawing.Size(32, 17);
            this.lbl_sifrecoz_plat.TabIndex = 22;
            this.lbl_sifrecoz_plat.Text = "Site";
            this.lbl_sifrecoz_plat.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_sifre_coz
            // 
            this.btn_sifre_coz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_sifre_coz.Location = new System.Drawing.Point(284, 88);
            this.btn_sifre_coz.Name = "btn_sifre_coz";
            this.btn_sifre_coz.Size = new System.Drawing.Size(108, 31);
            this.btn_sifre_coz.TabIndex = 22;
            this.btn_sifre_coz.Text = "Şifreyi Çek";
            this.btn_sifre_coz.UseVisualStyleBackColor = true;
            this.btn_sifre_coz.Click += new System.EventHandler(this.btn_sifre_coz_Click);
            // 
            // btn_girisYap
            // 
            this.btn_girisYap.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_girisYap.Location = new System.Drawing.Point(424, 88);
            this.btn_girisYap.Name = "btn_girisYap";
            this.btn_girisYap.Size = new System.Drawing.Size(108, 31);
            this.btn_girisYap.TabIndex = 23;
            this.btn_girisYap.Text = "Giriş Yap";
            this.btn_girisYap.UseVisualStyleBackColor = true;
            this.btn_girisYap.Click += new System.EventHandler(this.btn_girisYap_Click);
            // 
            // sifre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 450);
            this.Controls.Add(this.btn_girisYap);
            this.Controls.Add(this.btn_sifre_coz);
            this.Controls.Add(this.lbl_sifrecoz_plat);
            this.Controls.Add(this.cbx_sifrecoz_plat);
            this.Controls.Add(this.txt_sifrecoz_key2);
            this.Controls.Add(this.lbl_sifrecoz_key2);
            this.Controls.Add(this.lbl_sifrecozkey1);
            this.Controls.Add(this.txt_sifrecoz_key1);
            this.Controls.Add(this.grpbx_sifre);
            this.Name = "sifre";
            this.Text = "Giris_f";
            this.Load += new System.EventHandler(this.Giris_f_Load);
            this.grpbx_sifre.ResumeLayout(false);
            this.grpbx_sifre.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_s_mail;
        private System.Windows.Forms.Label lbl_s_mail;
        private System.Windows.Forms.Button btb_s_sifrele;
        private System.Windows.Forms.CheckBox chckbx_Affine;
        private System.Windows.Forms.GroupBox grpbx_sifre;
        private System.Windows.Forms.TextBox txt_s_sifre;
        private System.Windows.Forms.Label lbl_s_sifre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_s_key2;
        private System.Windows.Forms.TextBox txt_s_key1;
        private System.Windows.Forms.CheckBox chckbc_B_F;
        private System.Windows.Forms.CheckBox chckbx_RSA;
        private System.Windows.Forms.ComboBox pwdName;
        private System.Windows.Forms.Label lbl_text_no;
        private System.Windows.Forms.Label lbl_telNo;
        private System.Windows.Forms.Label lbl_sifrele_plat;
        private System.Windows.Forms.TextBox txt_sifrecoz_key1;
        private System.Windows.Forms.Label lbl_sifrecozkey1;
        private System.Windows.Forms.TextBox txt_sifrecoz_key2;
        private System.Windows.Forms.ComboBox cbx_sifrecoz_plat;
        private System.Windows.Forms.Label lbl_sifrecoz_key2;
        private System.Windows.Forms.Label lbl_sifrecoz_plat;
        private System.Windows.Forms.Button btn_sifre_coz;
        private System.Windows.Forms.Button btn_girisYap;
    }
}