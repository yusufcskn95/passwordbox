﻿namespace PasswordBoxv1._1
{
    partial class F_giris
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl_sure = new System.Windows.Forms.Label();
            this.btn_kayıtOl = new System.Windows.Forms.Button();
            this.lbl_kSure = new System.Windows.Forms.Label();
            this.btn_kodUret = new System.Windows.Forms.Button();
            this.lbl_kod = new System.Windows.Forms.Label();
            this.lbl_telNo = new System.Windows.Forms.Label();
            this.txt_kod = new System.Windows.Forms.TextBox();
            this.txt_tNo = new System.Windows.Forms.TextBox();
            this.btn_giris = new System.Windows.Forms.Button();
            this.timerforRnd = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lbl_sure
            // 
            this.lbl_sure.AutoSize = true;
            this.lbl_sure.Location = new System.Drawing.Point(363, 125);
            this.lbl_sure.Name = "lbl_sure";
            this.lbl_sure.Size = new System.Drawing.Size(19, 13);
            this.lbl_sure.TabIndex = 22;
            this.lbl_sure.Text = "00";
            // 
            // btn_kayıtOl
            // 
            this.btn_kayıtOl.Location = new System.Drawing.Point(271, 151);
            this.btn_kayıtOl.Name = "btn_kayıtOl";
            this.btn_kayıtOl.Size = new System.Drawing.Size(75, 23);
            this.btn_kayıtOl.TabIndex = 20;
            this.btn_kayıtOl.Text = "Kayıt Ol";
            this.btn_kayıtOl.UseVisualStyleBackColor = true;
            this.btn_kayıtOl.Click += new System.EventHandler(this.btn_kayıtOl_Click);
            // 
            // lbl_kSure
            // 
            this.lbl_kSure.AutoSize = true;
            this.lbl_kSure.Location = new System.Drawing.Point(295, 125);
            this.lbl_kSure.Name = "lbl_kSure";
            this.lbl_kSure.Size = new System.Drawing.Size(62, 13);
            this.lbl_kSure.TabIndex = 19;
            this.lbl_kSure.Text = "Kalan Süre:";
            // 
            // btn_kodUret
            // 
            this.btn_kodUret.Location = new System.Drawing.Point(192, 96);
            this.btn_kodUret.Name = "btn_kodUret";
            this.btn_kodUret.Size = new System.Drawing.Size(84, 23);
            this.btn_kodUret.TabIndex = 18;
            this.btn_kodUret.Text = "Kod Üret";
            this.btn_kodUret.UseVisualStyleBackColor = true;
            this.btn_kodUret.Click += new System.EventHandler(this.btn_kodUret_Click);
            // 
            // lbl_kod
            // 
            this.lbl_kod.AutoSize = true;
            this.lbl_kod.Location = new System.Drawing.Point(96, 132);
            this.lbl_kod.Name = "lbl_kod";
            this.lbl_kod.Size = new System.Drawing.Size(29, 13);
            this.lbl_kod.TabIndex = 16;
            this.lbl_kod.Text = "Kod:";
            // 
            // lbl_telNo
            // 
            this.lbl_telNo.AutoSize = true;
            this.lbl_telNo.Location = new System.Drawing.Point(96, 73);
            this.lbl_telNo.Name = "lbl_telNo";
            this.lbl_telNo.Size = new System.Drawing.Size(90, 13);
            this.lbl_telNo.TabIndex = 15;
            this.lbl_telNo.Text = "Telefon Numarası";
            // 
            // txt_kod
            // 
            this.txt_kod.Location = new System.Drawing.Point(192, 125);
            this.txt_kod.Name = "txt_kod";
            this.txt_kod.Size = new System.Drawing.Size(100, 20);
            this.txt_kod.TabIndex = 14;
            // 
            // txt_tNo
            // 
            this.txt_tNo.Location = new System.Drawing.Point(192, 66);
            this.txt_tNo.Name = "txt_tNo";
            this.txt_tNo.Size = new System.Drawing.Size(100, 20);
            this.txt_tNo.TabIndex = 13;
            // 
            // btn_giris
            // 
            this.btn_giris.Location = new System.Drawing.Point(190, 151);
            this.btn_giris.Name = "btn_giris";
            this.btn_giris.Size = new System.Drawing.Size(75, 23);
            this.btn_giris.TabIndex = 12;
            this.btn_giris.Text = "Giriş";
            this.btn_giris.UseVisualStyleBackColor = true;
            this.btn_giris.Click += new System.EventHandler(this.btn_giris_Click);
            // 
            // timerforRnd
            // 
            this.timerforRnd.Tick += new System.EventHandler(this.timerforRnd_Tick);
            // 
            // F_giris
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 342);
            this.Controls.Add(this.lbl_sure);
            this.Controls.Add(this.btn_kayıtOl);
            this.Controls.Add(this.lbl_kSure);
            this.Controls.Add(this.btn_kodUret);
            this.Controls.Add(this.lbl_kod);
            this.Controls.Add(this.lbl_telNo);
            this.Controls.Add(this.txt_kod);
            this.Controls.Add(this.txt_tNo);
            this.Controls.Add(this.btn_giris);
            this.Name = "F_giris";
            this.Text = "Password Box";
            this.Load += new System.EventHandler(this.F_giris_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_sure;
        private System.Windows.Forms.Button btn_kayıtOl;
        private System.Windows.Forms.Label lbl_kSure;
        private System.Windows.Forms.Button btn_kodUret;
        private System.Windows.Forms.Label lbl_kod;
        private System.Windows.Forms.Label lbl_telNo;
        private System.Windows.Forms.TextBox txt_kod;
        private System.Windows.Forms.TextBox txt_tNo;
        private System.Windows.Forms.Button btn_giris;
        private System.Windows.Forms.Timer timerforRnd;
    }
}

