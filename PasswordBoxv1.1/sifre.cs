﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Keys = OpenQA.Selenium.Keys;

namespace PasswordBoxv1._1
{
    public partial class sifre : Form
    {
        private string mesajcheckBox = "Şifrelemede kullanmak istediğiniz Key1 ve Key2'yi belirleyin";
        string sifresiz_sifrem;
        Encrypt crypt = new Encrypt();
        List<char> chArray = new List<char>();
        sqlBaglantisi baglanti = new sqlBaglantisi();
        Hata hata = new Hata();
        public sifre()
        {
            InitializeComponent();
        }
         public string telNo;

        private void chckbx_Affine_CheckedChanged(object sender, EventArgs e)
        {
            showMesaj(mesajcheckBox);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            showMesaj(mesajcheckBox);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            showMesaj(mesajcheckBox);
        }
        private void showMesaj(string mesaj) { MessageBox.Show(mesaj); }

        private void btb_s_sifrele_Click(object sender, EventArgs e)
        {
            if(chckbc_B_F.Checked==true && chckbx_Affine.Checked==true || chckbx_RSA.Checked == true)
            {
                showMesaj("Lutfen 1 tane seçin");
            }
            else if(chckbc_B_F.Checked==true || chckbx_Affine.Checked==true && chckbx_RSA.Checked == true)
            {
                showMesaj("Lutfen 1 tane seçin");
            }
            else if(chckbc_B_F.Checked==true && chckbx_Affine.Checked==true && chckbx_RSA.Checked==true)
            {
                showMesaj("Lutfen 1 tane Seçin");
            }
            else
            {
                if(chckbx_Affine.Checked == true)
                {
                    string sifre = txt_s_sifre.Text;    
                    
                    crypt.setA(Convert.ToInt32(txt_s_key1.Text));
                    crypt.setB(Convert.ToInt32(txt_s_key2.Text));
                    List<char> sifrelenecek = new List<char>();
                    sifre.ToList<char>();
                    chArray = sifre.ToList<char>();
                    
                   string str_sifreli =string.Join("", crypt.sifrele(chArray));
                    if (str_sifreli=="")
                    {
                        MessageBox.Show("Şifreleme Yapılamadı..!");
                    }
                    else
                    {
                        if (baglanti.SifreKaydet(str_sifreli, telNo, pwdName.SelectedItem.ToString(), txt_s_mail.Text))
                        {
                            MessageBox.Show("Şifreleme Başarılı..!");
                        }
                    }
                                    


                }
                else if (chckbc_B_F.Checked == true)
                {

                }
                else if(chckbx_RSA.Checked == true)
                {

                }
            }
            
        }

        private void Giris_f_Load(object sender, EventArgs e)
        {
            lbl_telNo.Text = telNo;
        }

        private void btn_sifre_coz_Click(object sender, EventArgs e)
        {
            string sifreli_sifrem;
            sifreli_sifrem = baglanti.get_pwd_where_pwdName(cbx_sifrecoz_plat.SelectedItem.ToString());
            
            sifresiz_sifrem= crypt.SifreCoz(sifreli_sifrem, Convert.ToInt32(txt_sifrecoz_key1.Text), Convert.ToInt32(txt_sifrecoz_key2.Text));
            MessageBox.Show("şifreniz : "+sifresiz_sifrem);

        }

        private void btn_girisYap_Click(object sender, EventArgs e)
        {
            if (cbx_sifrecoz_plat.SelectedItem.ToString() == "Facebook")
            {
                try
                {
                    IWebDriver driver = new FirefoxDriver();
                    driver.Url = "https://www.facebook.com/login/device-based/regular/login/?login_attempt=1&lwv=110";
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.FindElement(By.Id("email")).SendKeys(baglanti.get_mail_where_tNo(lbl_telNo.Text));
                    driver.FindElement(By.Id("pass")).SendKeys(sifresiz_sifrem + Keys.Enter);
                }
                catch (Exception hataMesaj)
                {
                    hata.hataGonderMesaj(hataMesaj.ToString());

                }


            }
            else if (cbx_sifrecoz_plat.SelectedItem.ToString() == "Piazza")
            {
                try
                {
                    IWebDriver driver = new FirefoxDriver();
                    driver.Url = "https://piazza.com/account/login";
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.FindElement(By.Id("email")).SendKeys(baglanti.get_mail_where_tNo(lbl_telNo.Text));
                    driver.FindElement(By.Id("password")).SendKeys(sifresiz_sifrem + Keys.Enter);
                }

                catch (Exception hataMesaj)
                {
                    hata.hataGonderMesaj(hataMesaj.ToString());

                }


            }




            else if (cbx_sifrecoz_plat.SelectedItem.ToString() == "Hotmail")
            {
                try
                {
                    IWebDriver driver = new FirefoxDriver();
                    driver.Url = "https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=13&ct=1546376402&rver=7.0.6737.0&wp=MBI_SSL&wreply=https%3a%2f%2foutlook.live.com%2fowa%2f%3fnlp%3d1%26RpsCsrfState%3db0cd5ccf-a075-c1ac-db49-b870fc4a128a&id=292841&CBCXT=out&lw=1&fl=dob%2cflname%2cwld&cobrandid=90015";
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.FindElement(By.Id("i0116")).SendKeys(baglanti.get_mail_where_tNo(lbl_telNo.Text) + Keys.Enter);
                    driver.FindElement(By.Id("i0118")).SendKeys(sifresiz_sifrem + Keys.Enter);
                }

                catch (Exception hataMesaj)
                {
                    hata.hataGonderMesaj(hataMesaj.ToString());

                }

            }
            else if (cbx_sifrecoz_plat.SelectedItem.ToString() == "BitBucket")
            {   
                // Hata için konuldu
                try
                {
                    IWebDriver driver = new FirefoxDriver();
                    driver.Url = "https://id.atlassian.com/login?continue=https%3A%2F%2Fid.atlassian.com%2Fopenid%2Fv2%2Fop%3Fopenid.return_to%3Dhttps%3A%2F%2Fbitbucket.org%2Fsocialauth%2Fcomplete%2Fatlassianid%2F%3Fjanrain_nonce%253D2019-01-03T00%25253A22%25253A41Zhhcdnm%26openid.sreg.optional%3Dfullname%2Cnickname%2Cemail%26openid.ns%3Dhttp%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%26openid.ns.sreg%3Dhttp%3A%2F%2Fopenid.net%2Fextensions%2Fsreg%2F1.1%26openid.crowdid.application%3Dbitbucket%26openid.assoc_handle%3D13582017%26openid.ns.crowdid%3Dhttps%3A%2F%2Fdeveloper.atlassian.com%2Fdisplay%2FCROWDDEV%2FCrowdID%252BOpenID%252Bextensions%2523CrowdIDOpenIDextensions-login-page-parameters%26openid.identity%3Dhttp%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select%26openid.realm%3Dhttps%3A%2F%2Fbitbucket.org%26openid.claimed_id%3Dhttp%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select%26openid.mode%3Dcheckid_setup&prompt=&application=bitbucket&tenant=&email=&errorCode=";
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.FindElement(By.Id("username")).SendKeys(baglanti.get_mail_where_tNo(lbl_telNo.Text) + Keys.Enter);
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                    driver.FindElement(By.Id("password")).SendKeys(sifresiz_sifrem + Keys.Enter);
                }

                catch (Exception hataMesaj)
                {
                    hata.hataGonderMesaj(hataMesaj.ToString());

                }

            }
            else
            {
                MessageBox.Show("Yanlış bir seçim yaptınız..!");
            }
        }
    }
}
