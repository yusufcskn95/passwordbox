﻿namespace PasswordBoxv1._1
{
    partial class Kayit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_k_telNo = new System.Windows.Forms.TextBox();
            this.lbl_k_telNo = new System.Windows.Forms.Label();
            this.btn_kayitol = new System.Windows.Forms.Button();
            this.lbl_k_mail = new System.Windows.Forms.Label();
            this.txt_k_mail = new System.Windows.Forms.TextBox();
            this.txt_d_Kodu = new System.Windows.Forms.TextBox();
            this.btn_dogrula = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_k_telNo
            // 
            this.txt_k_telNo.Location = new System.Drawing.Point(157, 72);
            this.txt_k_telNo.Margin = new System.Windows.Forms.Padding(4);
            this.txt_k_telNo.Name = "txt_k_telNo";
            this.txt_k_telNo.Size = new System.Drawing.Size(132, 23);
            this.txt_k_telNo.TabIndex = 0;
            // 
            // lbl_k_telNo
            // 
            this.lbl_k_telNo.AutoSize = true;
            this.lbl_k_telNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_k_telNo.Location = new System.Drawing.Point(25, 78);
            this.lbl_k_telNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_k_telNo.Name = "lbl_k_telNo";
            this.lbl_k_telNo.Size = new System.Drawing.Size(124, 17);
            this.lbl_k_telNo.TabIndex = 1;
            this.lbl_k_telNo.Text = "Telefon Numarasi:";
            this.lbl_k_telNo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_kayitol
            // 
            this.btn_kayitol.Location = new System.Drawing.Point(157, 147);
            this.btn_kayitol.Margin = new System.Windows.Forms.Padding(4);
            this.btn_kayitol.Name = "btn_kayitol";
            this.btn_kayitol.Size = new System.Drawing.Size(100, 28);
            this.btn_kayitol.TabIndex = 2;
            this.btn_kayitol.Text = "Kayıt Ol";
            this.btn_kayitol.UseVisualStyleBackColor = true;
            this.btn_kayitol.Click += new System.EventHandler(this.btn_kayitol_Click);
            // 
            // lbl_k_mail
            // 
            this.lbl_k_mail.AutoSize = true;
            this.lbl_k_mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_k_mail.Location = new System.Drawing.Point(68, 119);
            this.lbl_k_mail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_k_mail.Name = "lbl_k_mail";
            this.lbl_k_mail.Size = new System.Drawing.Size(81, 17);
            this.lbl_k_mail.TabIndex = 3;
            this.lbl_k_mail.Text = "Mail Adresi:";
            this.lbl_k_mail.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txt_k_mail
            // 
            this.txt_k_mail.Location = new System.Drawing.Point(157, 116);
            this.txt_k_mail.Margin = new System.Windows.Forms.Padding(4);
            this.txt_k_mail.Name = "txt_k_mail";
            this.txt_k_mail.Size = new System.Drawing.Size(132, 23);
            this.txt_k_mail.TabIndex = 4;
            // 
            // txt_d_Kodu
            // 
            this.txt_d_Kodu.Location = new System.Drawing.Point(157, 183);
            this.txt_d_Kodu.Margin = new System.Windows.Forms.Padding(4);
            this.txt_d_Kodu.Name = "txt_d_Kodu";
            this.txt_d_Kodu.Size = new System.Drawing.Size(132, 23);
            this.txt_d_Kodu.TabIndex = 5;
            this.txt_d_Kodu.Visible = false;
            // 
            // btn_dogrula
            // 
            this.btn_dogrula.Location = new System.Drawing.Point(157, 214);
            this.btn_dogrula.Margin = new System.Windows.Forms.Padding(4);
            this.btn_dogrula.Name = "btn_dogrula";
            this.btn_dogrula.Size = new System.Drawing.Size(100, 28);
            this.btn_dogrula.TabIndex = 6;
            this.btn_dogrula.Text = "Doğrula";
            this.btn_dogrula.UseVisualStyleBackColor = true;
            this.btn_dogrula.Visible = false;
            this.btn_dogrula.Click += new System.EventHandler(this.btn_dogrula_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(35, 189);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Doğrulama Kodu:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Kayit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 307);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_dogrula);
            this.Controls.Add(this.txt_d_Kodu);
            this.Controls.Add(this.txt_k_mail);
            this.Controls.Add(this.lbl_k_mail);
            this.Controls.Add(this.btn_kayitol);
            this.Controls.Add(this.lbl_k_telNo);
            this.Controls.Add(this.txt_k_telNo);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Kayit";
            this.Text = "Kayit";
            this.Load += new System.EventHandler(this.Kayit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_k_telNo;
        private System.Windows.Forms.Label lbl_k_telNo;
        private System.Windows.Forms.Button btn_kayitol;
        private System.Windows.Forms.Label lbl_k_mail;
        private System.Windows.Forms.TextBox txt_k_mail;
        private System.Windows.Forms.TextBox txt_d_Kodu;
        private System.Windows.Forms.Button btn_dogrula;
        private System.Windows.Forms.Label label1;
    }
}