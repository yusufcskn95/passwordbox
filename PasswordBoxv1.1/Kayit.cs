﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordBoxv1._1
{

    public partial class Kayit : Form
    {
        MailClass mail = new MailClass();
        sqlBaglantisi baglanti = new sqlBaglantisi();
        public Kayit()
        {
            InitializeComponent();
        }

        private void btn_kayitol_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Lutfen E-mail Adresinize Gelen Doğrulama Kodunu Giriniz!!");
            btn_dogrula.Visible = true;
            txt_d_Kodu.Visible = true;

            mail.send_Code(txt_k_mail.Text);
            
        }

        private void btn_dogrula_Click(object sender, EventArgs e)
        {
            if (txt_d_Kodu.Text == mail.get_rndCode().ToString())
            {            
                baglanti.user_add(txt_k_telNo.Text, txt_k_mail.Text);

                F_giris yeni = new F_giris();
                yeni.Show();
                this.Hide();
            }
        }

        private void Kayit_Load(object sender, EventArgs e)
        {

        }
    }
}
