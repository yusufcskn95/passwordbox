﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordBoxv1._1
{
    class Encrypt
    {
        private int keyA;
        private int keyB;
        private List<char> sifreli = new List<char>();
        private List<char> sifresiz = new List<char>();
        private int isPrime()
        {
            int kalan;
            if (keyA > keyB)
                for (int i = keyB; i < keyA; i++)
                {
                    kalan = keyA % i;
                    if (kalan == 0)
                    {
                        return -1;
                    }

                }
            if (keyA < keyB)
            {
                for (int i = keyA; i < keyB; i++)
                {
                    kalan = keyB % i;
                    if (kalan == 0)
                    {
                        return -1;
                    }
                }

            }
            else
                return 1;

            return 0;


        }
        Hata hata = new Hata();

        public List<char> sifrele(List<char> X)
        {
            sifreli.Clear();

            int kontrol = isPrime();
            if (kontrol == -1)
            {
                MessageBox.Show("Anahtar sayılarınız aralarında asal değil yeniden deneyiniz...!");
              
            }
            else
            {
                int kapasite = X.Count;
                for (int i = 0; i < kapasite; i++)
                {
                    sifreli.Add(Convert.ToChar(keyA * Convert.ToInt32(X[i]) + keyB));
                    
                }
            }

            return sifreli;
        }
        

        public void setA(int stKey)
        {
            keyA = stKey;

        }
        public int getA() { return keyA; }

        public void setB(int ndkey)
        {
            keyB = ndkey;
        }
        public int getB() { return keyB; }

        public string SifreCoz(string sifre, int key1, int key2)
        {
            string str_sifresiz;
            str_sifresiz = "";
            try
            {
                sifre.ToList<char>();
                //sifresiz = sifre.ToList<char>();

                for (int i = 0; i < sifre.Length; i++)
                {
                    sifresiz.Add(Convert.ToChar((Convert.ToInt32(sifre[i]) - key2) / key1));

                }
                str_sifresiz = string.Join("", sifresiz);
                sifresiz.Clear();
            }
            catch(Exception fail)
            {
                hata.hataGonderMesaj(fail.Message.ToString());

            }
            
            return str_sifresiz;
        }

    }
}
